import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../environments/environment'
@Injectable({
	providedIn: 'root'
})

export class LayoutService {

	constructor(private http: HttpClient) { }

	getJobs() {
		const path = environment.host + '/jobs';
		return this.http.get(path);
	}

	getjobDetails(id) {
		const path = environment.host + '/getjobDetails/' + id;
		return this.http.get(path);
	}

	jobSync() {
		const path = environment.host + '/jobSync';
		return this.http.get(path);
	}

	getJobsbySearch(search, location) {
		const path = environment.host + '/getJobsbySearch';
		return this.http.get(path, {
			params: {
				search: search,
				location: location
			}
		});
	}
}
