import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LayoutService } from './../app.servie';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatTable, MatSort, MatPaginator, MatTableDataSource, PageEvent } from '@angular/material';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  @ViewChild('input') input: ElementRef;
  @ViewChild('location') location: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  jobPosts: any = [];
  search: string;

  constructor(private http: HttpClient, private layoutService: LayoutService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.layoutService.getJobs().subscribe(res => {
      this.jobPosts = res['posts'];
      this.spinner.hide();
    });
  }

  detailedPage(id) {
    this.router.navigate(['details/' + id]);
  }

  searchbyDescriptor() {
    
    this.spinner.show();
    this.layoutService.getJobsbySearch(this.input.nativeElement.value, this.location.nativeElement.value).subscribe(res => {
      this.jobPosts = res['posts'];
      this.spinner.hide();
    });
  }

  syncJobs() {
    this.spinner.show();
    this.layoutService.jobSync().subscribe(res => {
      this.spinner.hide();
      this.jobPosts = res['posts'];
    });
  }

  clearSearch() {
    this.input.nativeElement.value = '';
    this.location.nativeElement.value = '';
  }

}
