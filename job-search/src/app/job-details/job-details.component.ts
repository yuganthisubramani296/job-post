import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LayoutService } from './../app.servie';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit {

  id: string;
  jobDetails: any;

  constructor(private activatedRoute: ActivatedRoute, private layoutService: LayoutService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.layoutService.getjobDetails(this.id).subscribe(res => {
      this.jobDetails = res;
    });
  }
}
